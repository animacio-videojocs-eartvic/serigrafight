﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class groundCheck : MonoBehaviour
{
    public GameObject pols;


    void OnTriggerEnter2D(Collider2D col)
    {
        //això fa que si el personatge fa colisió amb un material amb un tag de "Ground" apareixin particules de pols.
        if (col.gameObject.tag.Equals("Ground"))
        {
            Instantiate(pols, transform.position, pols.transform.rotation);
        }

    }
}
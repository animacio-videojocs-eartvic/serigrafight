﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class plataforma_spawner : MonoBehaviour
{
    
    public GameObject plataforma;
    public GameObject plataformaPrefab;

    public Transform plataforma_spawn;
    public float retraso;
    public float plataforma_retraso;

    public float respawnTime = 1.0f;
    private Vector2 screenBounds;


    void Start()
    {

    //espera a la variable retraso, luego pasado ese tiempo invoca el plataforma_lanzamiento, cada vez que se llame al metodo plataforma_lanzamiento, se pone 
    // esperando al plataforma_retraso.
    InvokeRepeating("plataforma_lanzamiento",retraso, plataforma_retraso);    

    }

    // Update is called once per frame
    void Update()
    {
        
    }

    void plataforma_lanzamiento()

    {
    Instantiate(plataforma, plataforma_spawn.position, plataforma_spawn.rotation);    

    }

        //funcion
    private void spawnEnemy(){
        GameObject a = Instantiate(plataformaPrefab) as GameObject;
        a.transform.position = new Vector2(Random.Range(-screenBounds.y,screenBounds.y), Random.Range(-screenBounds.x, screenBounds.x));
    }

    // respawn time

    IEnumerator plataformaWave(){
        while(true){
            yield return new WaitForSeconds(respawnTime);
            spawnEnemy();
        }
    }


    

}

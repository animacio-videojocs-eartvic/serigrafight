﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyFollow : MonoBehaviour
{
    public float speed;
    public float stoppingDistance;
    private Transform target;
    private bool facingRight = false;

     public bool isAlive = true;
    Animator anim;
    public GameObject otherObject;

    public ParticleSystem caositoParticles;
    public ParticleSystem caositoDeathParticles;
    public float hp;





    void Start()
    {
        target = GameObject.FindGameObjectWithTag("Player").GetComponent<Transform>();
        
        anim = otherObject.GetComponent<Animator>();
        anim.SetInteger("Death", 0);
        caositoDeathParticles.Stop();




    }

   
    void Update()

    {

        //Debug.Log(hp);

        //Debug.Log(isAlive);

        //aqui estic fent que si la distancia entre en Cero i en Caosito es suficient, aquest últim persegueixi en Cero.
        if (isAlive)
        {



            if (Vector2.Distance(transform.position, target.position) > stoppingDistance)
            {
                transform.position = Vector2.MoveTowards(transform.position, target.position, speed * Time.deltaTime);
                if (target.position.x > transform.position.x && !facingRight)
                    Flip();
                if (target.position.x < transform.position.x && facingRight)
                    Flip();


            }


        }
        else
        {
            //Debug.Log("esticMort");
        }

       





    }



    //aqui dic que quan en caosito sempre estigui mirant al personatge 
        void Flip()
        {
            Vector3 scale = transform.localScale;
            scale.x *= -1;
            transform.localScale = scale;
            facingRight = !facingRight;
        }


    void OnTriggerEnter2D(Collider2D col)
    {

        //aqui dic com es comportarà el caosito al rebre mal de la pistola d'aigua.
        if (col.tag == "Water" && isAlive == true && hp > 0)
        {

            hp = hp - 1;

                StartCoroutine(damageColor());

            if (hp == 0)
            {
                //Destroy(col.gameObject.GetComponent<Collider>());
                StartCoroutine(Dietime());

            }





        }





    }

    IEnumerator Dietime()
    {
        //això activa la rutina de morir del caosito.
        
        anim.SetInteger("Death", 1);
        caositoParticles.Stop();

        isAlive = false;
        yield return new WaitForSeconds(1);
        caositoDeathParticles.Play();
        yield return new WaitForSeconds(0.40f);
        caositoDeathParticles.Stop();
        yield return new WaitForSeconds(0.60f);

        Destroy(gameObject);
    }


    IEnumerator damageColor()
    {
        //això serveix per canviar el color de caosito quan li fan mal, però li fa a tots els caositos de la escena, s'ha de revisar.
        foreach (GameObject sprite in GameObject.FindGameObjectsWithTag("Enemy"))
        {

            if(sprite.transform.IsChildOf(this.transform))
                sprite.GetComponent<SpriteRenderer>().material.color = new Color(1.0f, 0.5f, 0.5f, 1f);
        }

        yield return new WaitForSeconds(0.1f);

       
        // Debug.Log("inv false");
        foreach (GameObject sprite in GameObject.FindGameObjectsWithTag("Enemy"))
        {
            if(sprite.transform.IsChildOf(this.transform))
                sprite.GetComponent<SpriteRenderer>().material.color = new Color(1.0f, 1.0f, 1.0f, 1f);
        }



    }





}

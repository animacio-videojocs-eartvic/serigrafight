﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RotacioPersonatge : MonoBehaviour
{
    // Start is called before the first frame update
    //creo una booleana que em diu si està mirant cap a la dreta o a l'esquerra
    public static bool mirarDreta = true;
    public int health;
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        health = GetComponent<ceroLife>().health;


        if (health > 0)
        {

            // amb això faig tracking de la posició del personatge i el ratolí
            var delta = Camera.main.ScreenToWorldPoint(Input.mousePosition) - transform.position;

            if (delta.x >= 0 && !mirarDreta)
            { // el ratoli està a la part dreta del personatge
                transform.localScale = new Vector3(0.35f, 0.35f, 1);
                mirarDreta = true;
            }
            else if (delta.x < 0 && mirarDreta)
            { // el ratoli està a la part esquerra del personatge
                transform.localScale = new Vector3(-0.35f, 0.35f, 1);
                mirarDreta = false;

            }
        }
    }
}

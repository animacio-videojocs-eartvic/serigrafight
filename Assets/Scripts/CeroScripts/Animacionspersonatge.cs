﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Animacionspersonatge : MonoBehaviour
{

public bool tmpbool = true;
  private Rigidbody2D rb;
  public float speed;
  private float moveInput;
  private bool isGrounded;
  private bool xocLateral = false;
  public Transform feetPos;

  public Transform colliderLateral;

  public Vector2 nearRadius;
  public float checkRadius;
  public LayerMask whatIsGround; 
  public float jumpForce;
  private float jumpTimeCounter;
  public float jumpTime;
  private bool isJumping;

  public int health;



   


    bool mirarDreta;
    Animator anim;


    
    void Start()
    {
        rb = GetComponent<Rigidbody2D>();
        anim = GetComponent<Animator>();
        





    }

    void FixedUpdate()
    {

        // Debug.Log(xocLateral);
        //Debug.Log(isGrounded);

 
        //nose perquè això no em funciona
        


        health = GetComponent<ceroLife>().health;
 
        if (health > 0)
        {


            moveInput = Input.GetAxis("Horizontal");
            if (xocLateral)
            {
                // moveInput = 0;
            }
            rb.velocity = new Vector2(moveInput * speed, rb.velocity.y);

            //aqui creo totes les condicions perque les animacions s'iniciin correctament

            if (isGrounded == true)
            {
                anim.SetBool("Grounded", true);


            }
            else
            {
                anim.SetBool("Grounded", false);


            }

            float horizontal = Input.GetAxis("Horizontal");

            if (horizontal > 0 && mirarDreta == true && isGrounded == true)
            {

                anim.SetInteger("direcció", 1);
            }




            if (horizontal < 0 && mirarDreta == true && isGrounded == true)
            {
                anim.SetInteger("direcció", -1);
            }


            if (horizontal > 0 && mirarDreta == false && isGrounded == true)
            {

                anim.SetInteger("direcció", -1);


            }





            if (horizontal < 0 && mirarDreta == false && isGrounded == true)
            {
                anim.SetInteger("direcció", 1);

            }


            if (horizontal == 0)
            {
                anim.SetInteger("direcció", 0);

            }




        }
    }

    void Update()
    {
        health = GetComponent<ceroLife>().health;

        if (health > 0)
        {




            //aqui creo un cercle sota del personatge que si té colisió amb el terra activara o desactivara les animacions de idle o salt.

            isGrounded = Physics2D.OverlapCircle(feetPos.position, checkRadius, whatIsGround);
            xocLateral = Physics2D.OverlapBox(colliderLateral.position, nearRadius, 0f, whatIsGround);
            tmpbool = xocLateral;
            if (isGrounded && Input.GetKeyDown(KeyCode.Space))
            {
                isJumping = true;
                jumpTimeCounter = jumpTime;
                rb.velocity = Vector2.up * jumpForce;
            }

            if (Input.GetKey(KeyCode.Space) && isJumping && !isGrounded)
            {

                if (jumpTimeCounter > 0)
                {
                    rb.velocity = Vector2.up * jumpForce;
                    jumpTimeCounter -= Time.deltaTime;
                }
                else
                {
                    isJumping = false;


                }




            }

            if (Input.GetKeyUp(KeyCode.Space))
            {
                isJumping = false;
            }

            mirarDreta = RotacioPersonatge.mirarDreta;







        }
    }
}

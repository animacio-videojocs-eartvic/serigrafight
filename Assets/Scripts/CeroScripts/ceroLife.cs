﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ceroLife : MonoBehaviour
{
    public int health;
    public int numOfLives;

     public Image[] livesImage;
    public Sprite livesSprite;
    public Sprite hollowLivesSprite;


    private bool invincible = false;
    public float invincibilityTime;

    public float knockback;
    public float knockbackLength;
    public float knockbackCount;
    public bool knockFromRight;

    public Transform enemyPosition;

    private Rigidbody2D rb;
    Color c;
    Renderer rend;
    //public GameObject CEROFINAL;
    Animator anim;
    void Start()
    {


      

        anim = GetComponent<Animator>();
        rb = GetComponent<Rigidbody2D>();


    }

    // Update is called once per frame
    void Update()
    {
        // Debug.Log(lives);

        //Debug.Log(health);

        if(health > numOfLives)
        {
            health = numOfLives;
        }



        for (int i = 0; i < livesImage.Length; i++)
        {
            if (i < health)
            {
                livesImage[i].sprite = livesSprite;
            }
            else
            {
                livesImage[i].sprite = hollowLivesSprite;
            }


            if(i < numOfLives)
            {
                livesImage[i].enabled = true;
            }
            else
            {
                livesImage[i].enabled = false;
            }
        }



    }



    void OnTriggerStay2D(Collider2D col)
    {
           

          

        //això fa que si el personatge fa colisió amb un material amb un tag de "Enemy" perdi vida.
        if (invincible == false)
        {
            if (col.gameObject.tag.Equals("Enemy") && health > 0)
            {

                


                health -= 1;
                if(health == 0){

                    anim.SetTrigger("Death");
                    
                }

               /*  knockbackCount = knockbackLength;

                if(enemyPosition.transform.position.x < transform.position.x){

                    knockFromRight = true;
                }else{
                    knockFromRight = false;
                }

                 if(knockFromRight){
                    rb.velocity = new Vector2 (-knockback * knockback, 0);
                }
                 
                if(!knockFromRight){
                    rb.velocity = new Vector2 (knockback * knockback, 0);
                    knockbackCount -= Time.deltaTime;
                }
                 */

                StartCoroutine(Invulnerability());

               

                //CEROFINAL.GetComponent<SpriteRenderer>().material.color = new Color(1.0f, 1.0f, 1.0f, 0.5f);





            }


            
               

        }



        IEnumerator Invulnerability()
        {
            invincible = true;
            //Debug.Log("inv true");
            anim.SetTrigger("Hurt");


            foreach (GameObject sprite in GameObject.FindGameObjectsWithTag("sprites"))
            {
               
                sprite.GetComponent<SpriteRenderer>().material.color = new Color(1.0f, 0.5f, 0.5f, 1f);
            }

            yield return new WaitForSeconds(invincibilityTime);

            invincible = false;
           // Debug.Log("inv false");
            foreach (GameObject sprite in GameObject.FindGameObjectsWithTag("sprites"))
            {
                
                sprite.GetComponent<SpriteRenderer>().material.color = new Color(1.0f, 1.0f, 1.0f, 1f);
            }


        }



    }
}

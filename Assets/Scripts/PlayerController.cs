﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class PlayerController : MonoBehaviour

{



/* VARIABLES COMPONENTES
   Variables que necesitaremos para acceder al rigidbody2d,al animator del unity, 
*/

private Rigidbody2D rb2d;
private Animator animador;
private PlayerController player;



 /* VARIABLES MOVIMIENTO
    Variables para la velocidad del jugador y si toca el suelo que la necesitaremos para la animacion del salto.
 */

public float speed = 10f;
public float maxspeed = 20f;
public bool ground;
public bool ring;
public bool muerto = false;

// VARIABLES SALTO
//variables para el salto, una para el maximo de potencia de salto y otra para saber si se ha activado y no volver a repetir el salto en el aire.

public float JumpPower = 10f;
private bool Jump;


// FUNCION START
void Start()

{

//Hacemos que la variable rgb2d almacena los datos del rigidbody2d del menu de unity, igual el resto de variables para el animator, playercontroller y audiosource.

rb2d = GetComponent<Rigidbody2D>();
animador = GetComponent<Animator>();
player = GetComponent<PlayerController>();



       
}

// Update is called once per frame
void Update()
    
{
// este parametro es para decirle a las condiciones puestas en el animador. hay que decirle el nombre exacto y luego el valor. El math.abs hara que no tenga en cuenta
// el valor negativo porque en las transiciones del animador las condiciones que hemos puesto son todas positivas. Ejemplo nuestro caso: mayor que 0.1 o menor a 0.1 por
// eso tenemos que añadir este parametro.

animador.SetFloat("Speed", Mathf.Abs(rb2d.velocity.x));
animador.SetBool("Ground", ground);


// Este comando getkeydown es cuando se pulsa una vez la tecla, es una boleana que devuelve true cuando has pulsado la tecla, se usa en update.

if (Input.GetKeyDown(KeyCode.Space) && ground)
        
{
    
    Jump = true;

}

 
if (Input.GetKeyDown("w"))
        
{
//Debug.Log("Eh pulsado");
animador.Play("Player_Look_Up");

}


}

//Funciona apartir del framerate del juego.
void FixedUpdate()

{
    
// El getaxis da un valor entre -1 o 1 por eso ponemos un float llamado h. Hace que detecte las teclas por defecto de unity que son horizontales.
float h = Input.GetAxis("Horizontal");
float y = Input.GetAxis("Vertical");
//Añadimos una fuerza de vector 2
rb2d.AddForce(Vector2.right * speed *h);



    // Hacemos un condicional para LIMITAR la VELOCIDAD del JUGADOR.. que le velocidad x es mayor al maxspeed. hacemos que la variable rb2d velocity adquiera
    // un nuevo vector2 donde ahi se pondra la velocidad maxima predefinida en el eje "x" y en el eje "y" por defecto. En este caso para la derecha.

    if(rb2d.velocity.x > maxspeed)

    {
        rb2d.velocity = new Vector2(maxspeed,rb2d.velocity.y);
    }
    // Hacemos lo mismo pero en este caso sera negativo porque sera hacia la izquierda.
    if(rb2d.velocity.x < -maxspeed)

    {
        rb2d.velocity = new Vector2(-maxspeed,rb2d.velocity.y);
    }
    //Debug.Log(rb2d.velocity.x);

    // la h nos da el movimiento y da un valor entre -1 o 1, si hacemos una comprovacion aqui haremos que gire el muñeco al ser negativo. Ahi que canviar del transform
    // el localScale a -1 para que el sprite gire a la izquierda.

    if(h > 0.1)

    {
    // pestaña transform tenemos que añadir vector 3 porque esta formado asin en el menu de unity. Valores cogidos de alli.    
    transform.localScale = new Vector3 (1f,1f,1f);
    }

    if(h < 0)

    {
    transform.localScale = new Vector3 (-1f,1f,1f);
    }

// si jump es verdadero, no ponemos si es igual a true porque ya le hemos metido el valor.
    if(Jump)
    {
    rb2d.AddForce(Vector2.up * JumpPower, ForceMode2D.Impulse);
// cuando hayamos saltado lo ponemos a false, para que no se repita el salto.
    Jump = false;

    }


    // 


    }


// funcion para cuando detecta una colision, lo hazemos para saber cuando esta tocando el suelo o no. Y usando Stay se refresca todo el rato.
void OnCollisionStay2D(Collision2D col)

{
player.ground = true;

}

// funcion cuando sale de una colision. Hazemos que el suelo sea falso.
void OnCollisionExit2D(Collision2D col)

{   
player.ground = false;
}



}

  
    













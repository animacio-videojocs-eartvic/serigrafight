﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MouseGunControl : MonoBehaviour
{
    // Start is called before the first frame update

        //aquest script farà que l'arma segueixi al ratolí

    Vector2 mousePosition;
    public ParticleSystem particles; //aigua
    public int health;
    public Transform mirilla;


    
  public GameObject goCero;

    void Start()
    {
        Cursor.visible = false; //la mirilla es canvia pel cursor
        particles.Stop();
    }

    // Update is called once per frame
    void Update()
    {

        health = goCero.GetComponent<ceroLife>().health;

        if(health > 0){
        mousePosition = Camera.main.ScreenToWorldPoint(Input.mousePosition);
        transform.position = mousePosition;

        if(Input.GetMouseButtonDown(0))
        {
            particles.Play();

        }
        if(Input.GetMouseButtonUp(0))
        {
            particles.Stop();

        }
        }
        else
        {
           
            StartCoroutine(Dietime());
        }


    }



    IEnumerator Dietime()
    {

        

        Cursor.visible = true;
        particles.Stop();

        yield return new WaitForSeconds(1);


        transform.position = new Vector2(1000, -28);
       
    }

}
